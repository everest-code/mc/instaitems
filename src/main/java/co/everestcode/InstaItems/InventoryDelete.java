package co.everestcode.InstaItems;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.inventory.Inventory;
import org.jetbrains.annotations.NotNull;

public class InventoryDelete implements CommandExecutor, Runnable {
    private Inventory inv;

    public InventoryDelete() {
        this.inv = Bukkit.createInventory(null, 9);
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        if (commandSender instanceof Player p) {
            p.openInventory(this.inv);
        }
        return true;
    }

    @Override
    public void run() {
        this.inv.clear();
    }
}
