package co.everestcode.InstaItems;

import co.everestcode.InstaItems.Listeners.ItemListener;
import co.everestcode.InstaItems.Listeners.PlayerDeadListener;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public class Main extends JavaPlugin {
    @Override
    public void onEnable() {
        File file = new File(getDataFolder(), "config.yml");
        if (!file.exists()) {
            saveDefaultConfig();
        } else {
            try {
                getConfig().load(file);
            } catch (IOException | InvalidConfigurationException e) {
                throw new RuntimeException(e);
            }
        }

        Logger log = getLogger();

        Listener item = new ItemListener(log, getConfig().getInt("search_radius"));
        getServer().getPluginManager().registerEvents(item, this);

        Listener dead = new PlayerDeadListener(log, getConfig().getBoolean("chest_on_dead"));
        getServer().getPluginManager().registerEvents(dead, this);

        InventoryDelete ir = new InventoryDelete();

        getServer().getScheduler().runTaskTimer(this, ir, 5L, 5L);
        getCommand("irem").setExecutor(ir);
        getCommand("item-remove").setExecutor(ir);
        getCommand("itemrem").setExecutor(ir);
        getCommand("iir").setExecutor(ir);

        log.info("Initializing InstaItem!!");
    }
}
