package co.everestcode.InstaItems.Listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Objects;
import java.util.logging.Logger;

public class PlayerDeadListener implements Listener {
    private final Logger logger;
    private final boolean chestOnDead;
    public PlayerDeadListener(Logger pLogger, boolean pChestOnDead) {
        logger = pLogger;
        chestOnDead = pChestOnDead;
    }

    private static boolean chestIsFull(Inventory chest) {
        for (ItemStack item: chest.getContents()) {
            if (item == null || item.getType() == Material.AIR) {
                return false;
            }
        }

        return true;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDeath(PlayerDeathEvent evt) {
        if (!chestOnDead) {
            evt.setKeepInventory(true);
            return;
        }

        evt.setKeepInventory(false);
        Player p = evt.getEntity();
        boolean isInLava = Objects.requireNonNull(p.getLastDamageCause()).getCause().equals(EntityDamageEvent.DamageCause.LAVA);
        if (isInLava) {
            p.sendMessage(String.format("\u00A7l\u00A74ALERT:\u00A7r \u00A7n\u00A7lSorry %s, your things are swimming in lava. can't be recovered :c", p.getName()));
            return;
        }

        if (!p.getInventory().isEmpty()) {
            Location chestLoc = p.getLocation().add(1, 0, 0);
            chestLoc.getBlock().setType(Material.CHEST);
            Inventory chest = ((Chest) chestLoc.getBlock().getState()).getBlockInventory();

            ItemStack[] inventory = p.getInventory().getContents();
            inventory = Arrays.copyOf(inventory, inventory.length);

            p.getInventory().clear();

            p.sendMessage("\u00A73\u00A7lNOTE:\u00A7r \u00A7l\u00A7nYour inventory was recovered, a chest with your inventory is waiting for you on your death place.");
            for (ItemStack item: inventory) {
                if (chestIsFull(chest)) {

                }

                if (item != null && item.getType() != Material.AIR) {
                    chest.addItem(item);
                }
            }
        }
    }
}
