package co.everestcode.InstaItems.Listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Hopper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.InventoryHolder;

import java.util.Objects;
import java.util.logging.Logger;

public class ItemListener implements Listener {
    private final Logger logger;
    private final int searchRadius;

    public ItemListener(Logger pLogger, int pSearchRadius) {
        logger = pLogger;
        searchRadius = pSearchRadius;
    }


    private Entity getNearInventoryHolder(Location loc) {
        return getNearInventoryHolder(loc, searchRadius, null);
    }

    private Entity getNearInventoryHolder(Location loc, Entity exclude) {
        return getNearInventoryHolder(loc, searchRadius, exclude);
    }

    private static Entity getNearInventoryHolder(Location iLoc, int radius, Entity exclude) {
        World w = Objects.requireNonNull(iLoc.getWorld());
        Entity nearestEntity = null;
        for (Entity e: w.getNearbyEntities(iLoc, radius, radius, radius)) {
            if (exclude != null) {
                if (e.getEntityId() == exclude.getEntityId()) {
                    continue;
                }
            }

            if (e instanceof InventoryHolder) {
                if (nearestEntity == null) {
                    nearestEntity = e;
                } else {
                    double diffNearEntity = nearestEntity.getLocation().distanceSquared(iLoc);
                    double diffEntity = e.getLocation().distanceSquared(iLoc);

                    if (diffEntity < diffNearEntity) {
                        nearestEntity = e;
                    }
                }
            }
        }

        return nearestEntity;
    }

    private Block getNearBlock(Location loc, Material material) {
        return getNearBlock(loc, searchRadius, material);
    }

    private Block getNearBlock(Location loc, int radius, Material material) {
        int minX = loc.getBlockX() - radius;
        int maxX = loc.getBlockX() + radius;
        int minZ = loc.getBlockZ() - radius;
        int maxZ = loc.getBlockZ() + radius;
        int minY = loc.getBlockY() - radius;
        int maxY = loc.getBlockY() + radius;

        World w = loc.getWorld();
        Block near = null;

        for (int y = minY; y <= maxY; y++) {
            for (int x = minX; x <= maxX; x++) {
                for (int z = minZ; z <= maxZ; z++) {
                    Block block = w.getBlockAt(x, y, z);
                    if (block.getType() == material) {
                        logger.info(String.format("Block %s find at %s", material, block.getLocation()));
                        if (near == null) {
                            near = block;
                        } else {
                            double oldDistance = near.getLocation().distanceSquared(loc);
                            double newDistance = block.getLocation().distanceSquared(loc);
                            if (newDistance < oldDistance) {
                                near = block;
                            }
                        }
                    }
                }
            }
        }

        return near;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerDropItem(PlayerDropItemEvent evt) {
        Player p = evt.getPlayer();
        Item i = evt.getItemDrop();

        Location loc = i.getLocation();
        i.remove();

        Entity nearestEntity = getNearInventoryHolder(loc, p);

        if (nearestEntity == null) {
            Block nearHopper = getNearBlock(loc, Material.HOPPER);
            if (nearHopper == null) {
                logger.warning(String.format("Player %s try to drop item", p.getName()));
                p.sendMessage(String.format("\u00A74\u00A7lALERT:\u00A7r \u00A7l\u00A7nHey %s, drop items is not able on this server.", p.getName()));
                p.getInventory().addItem(i.getItemStack());
                return;
            }

            ((Hopper) nearHopper.getState()).getInventory().addItem(i.getItemStack());
        } else {
            InventoryHolder holder = (InventoryHolder) nearestEntity;
            holder.getInventory().addItem(i.getItemStack());
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onItemSpawn(ItemSpawnEvent evt) {
        Item i = evt.getEntity();
        i.remove();

        Location iLoc = evt.getLocation();
        Entity nearestEntity = getNearInventoryHolder(iLoc);

        if (nearestEntity == null) {
            Block nearHopper = getNearBlock(iLoc, Material.HOPPER);
            if (nearHopper == null) {
                logger.warning(String.format("Item %s will be removed from '%s'", i.getName(), iLoc.getWorld().getName()));
                return;
            }

            ((Hopper) nearHopper.getState()).getInventory().addItem(i.getItemStack());
        } else {
            InventoryHolder holder = (InventoryHolder) nearestEntity;
            holder.getInventory().addItem(i.getItemStack());
        }
    }
}
